AUFS2_UTIL_VERSION = aufs2.1
AUFS2_UTIL_SITE = git://aufs.git.sourceforge.net/gitroot/aufs/aufs2-util.git
AUFS2_UTIL_SITE_METHOD = git

AUFS2_UTIL_CFLAGS = -I$(LINUX_HEADERS_DIR)/include $(TARGET_CFLAGS)

define AUFS2_UTIL_BUILD_CMDS
	#$(MAKE) -C $(@D) CFLAGS="$(AUFS2_UTIL_CFLAGS)" CC="$(TARGET_CC)" LD="$(TARGET_LD)" all
	$(MAKE) -C $(@D) CFLAGS="$(AUFS2_UTIL_CFLAGS) -Ilibau"
endef


define AUFS2_UTIL_INSTALL_TARGET_CMDS
	$(MAKE) -C $(@D) DESTDIR=$(TARGET_DIR) install
endef

$(eval $(call GENTARGETS,package,aufs2-util))
