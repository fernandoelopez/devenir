//		Copyright (C) 2011-2012  Fernando López fernando.e.lopez_AT_gmail.com
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.


#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/mount.h>

#define MKDIR(path) if (mkdir((path), 0755) != 0 && errno != EEXIST) { perror("Creating directory"); exit(-1); }
#define TRY(expr) if ((expr) != 0) { perror(#expr); exit(-1); }

#define FILE_BUFF 1024

enum { ROOT, RO, INIT, NFS, IP, QUIET, DEBUG, OPTS_SIZE };
char *cmd_opts_names[] = { "root", "ro", "init", "nfs", "ip", "quiet", "debug" };
char *cmd_opts[OPTS_SIZE];

int setup_initrd(){
	MKDIR("/proc");
	MKDIR("/sys");
	MKDIR("/dev");
	TRY(mount(NULL, "/proc", "proc", 0, NULL));
	TRY(mount(NULL, "/sys", "sysfs", 0, NULL));
	TRY(mount(NULL, "/dev", "tmpdevfs", 0, NULL));
	MKDIR("/dev/pts");
	TRY(mount(NULL, "/dev/pts", "devpts", 0, NULL));
	
	return 0;
}

char *get_cmdline(){
	FILE *f = fopen("/proc/cmdline", "r");
	int chars = FILE_BUFF;
	char *buff = malloc((FILE_BUFF + 1) * sizeof(char));
	while (chars == fread(buff, sizeof(char), FILE_BUFF, f)){
		chars *= 2;
		buff = realloc(buff, (chars + 1) * sizeof(char));
	}
	fclose(f);
	return buff;
}



int main(){
	setup_initrd();
	system("/bin/sh");

	return 0;
}
