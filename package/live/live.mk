AUFS2_UTIL_VERSION = aufs2.1
AUFS2_UTIL_SITE = git://aufs.git.sourceforge.net/gitroot/aufs/aufs2-util.git
AUFS2_UTIL_SITE_METHOD = git


define AUFS2_UTIL_BUILD_CMDS
	$(MAKE) KERNEL_HEADERS=$(KERNEL_HEADERS) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) all
endef

#define AUFS2_UTIL_INSTALL_STAGING_CMDS
#	$(MAKE) DESTDIR=$(STAGING_DIR) install
#endef

define AUFS2_UTIL_INSTALL_TARGET_CMDS
	$(MAKE) DESTDIR=$(TARGET_DIR) install
endef

$(eval $(call GENTARGETS,package,aufs2-util))
