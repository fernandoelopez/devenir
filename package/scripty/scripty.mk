SCRIPTY_VERSION = 0.2
#AUFS2_UTIL_SITE = git://aufs.git.sourceforge.net/gitroot/aufs/aufs2-util.git
#AUFS2_UTIL_SITE_METHOD = git



#define AUFS2_UTIL_INSTALL_STAGING_CMDS
#	$(MAKE) DESTDIR=$(STAGING_DIR) install
#endef

define SCRIPTY_INSTALL_TARGET_CMDS
	$(MAKE) DESTDIR=$(TARGET_DIR) install
endef

$(eval $(call GENTARGETS,package,scripty))
