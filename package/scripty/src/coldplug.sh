#!/bin/sh
#		Copyright (C) 2011-2012  Fernando López fernando.e.lopez_AT_gmail.com
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

parsedPCIMap=$(mktemp)
parsedUSBMap=$(mktemp)

echo "Coldplug..."

cat /lib/modules/$(uname -r)/modules.pcimap | grep -v "^#" | tr -s ' '| \
	cut -d' ' -f1-3 > $parsedPCIMap


cat /lib/modules/$(uname -r)/modules.usbmap | grep -v "^#" | tr -s ' '| \
	cut -d' ' -f1,3,4 > $parsedUSBMap


toPCImap(){ printf "0x0000%s 0x0000%s" $(echo $1 | tr : ' '); }
toUSBmap(){ printf "0x%s 0x%s" $(echo $1 | tr : ' '); }

# Todos los dispositivos PCI salvo placas de vídeo que no me
# interesan por el momento
pcidevs=$(lspci -n | cut -d' ' -f2,3 | grep -v ^0300: | cut -d' ' -f2)

for dev in $pcidevs; do
	mod=$(grep "$(toPCImap $dev)" $parsedPCIMap | cut -d' ' -f1)
	if [ -z "$mod" ]; then
		#echo "Módulo para dispositivo PCI: $dev no encontrado"
		continue
	fi
	modprobe $mod
done		

# Dispositivos usb
modprobe usbcore
for v in o e u; do
	modprobe ${v}hci-hcd
done
modprobe usb-storage

usbdevs=$(lsusb  | cut -d' ' -f6)

for dev in $usbdevs; do
	mod=$(grep "$(toUSBmap $dev)" $parsedUSBMap | cut -d' ' -f1)
	if [ -z "$mod" ]; then
		#echo "Módulo para dispositivo USB: $dev no encontrado"
		continue
	fi
	modprobe $mod
done		

# No entiendo porque es necesario lo siguiente para levantar todos
# los módulos...
find /dev -type b -exec dd if={} of=/dev/null count=1 \; 2> /dev/null
