#!/bin/sh
#		Copyright (C) 2011-2012  Fernando López fernando.e.lopez_AT_gmail.com
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

MARKER_MAX_DEPTH=2
MARKER_NAME=scripty.mrk
ORIGIN_FS_REGEX='(v?fat|ext[0-9])'
LOG_FILE=/dev/null
MOUNT_POINT=/mnt


find_marker_file(){
	find "$1" -maxdepth $MARKER_MAX_DEPTH -name "$MARKER_NAME" | head -n1
}

find_origin_fs_blkid(){
	blkid | while read line; do
		local TYPE
		local device=$(echo $line | cut -d: -f1)
		eval $(echo $line | grep -E ' TYPE=\"'$ORIGIN_FS_REGEX'\"')
		if [ -z "$TYPE" ]; then
			# Not supported origin fs, tune ORIGIN_FS_REGEX to fix this
			continue
		fi
		echo "Triyng to mount $device as $TYPE"
		mount -t $TYPE $device "$MOUNT_POINT" || continue
		local marker_file=$(find_maker_file "$MOUNT_POINT")
		if [ -n "$marker_file" ]; then
			echo $marker_file
			return
		fi
	done
}

find_origin_fs(){
	local device partitions mark
	for each in /sys/block/*; do
		device=$(basename $each)
		if [ -n "$(echo $device | grep -E '(ram|loop)[0-9]+$')" ]; then
			continue
		fi
		partitions=$(ls $each/ | grep -oE $device[0-9]+)
		if [ -z "$partitions" ]; then
			partitions=$device
		fi
		for fs in $partitions; do
			TYPE=$(blkid -o value -s TYPE /dev/$fs)
			mount -t $TYPE /dev/$fs "$MOUNT_POINT" || continue
			mark=$(find_marker_file "$MOUNT_POINT")
			if [ -n "$mark" ]; then
				echo $mark
				return
			fi
			umount "$MOUNT_POINT"
		done
	done
}

run_script(){
	#tail -f "$LOG_FILE"
	#sh "$@" > "$LOG_FILE"
	sh "$@"
}
