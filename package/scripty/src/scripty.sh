#!/bin/sh
#		Copyright (C) 2011-2012  Fernando López fernando.e.lopez_AT_gmail.com
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

. /usr/lib/scripty/functions.sh

MOUNT_POINT=/mnt/scripty
#####mount -t tmpfs -o size=1M tmpfs /mnt
mkdir "$MOUNT_POINT"

local_boot(){
	attemp=1
	ORIGIN=$(find_origin_fs)
	while [ -z "$ORIGIN" ] && [ $attemp -lt 3 ]; do
		echo "No se encontró nada, esperando $attemp segundo(s)..."
		sleep $attemp
		ORIGIN=$(find_origin_fs)
		let attemp++
	done

	if [ -z "$ORIGIN" ]; then
		while true; do
			getty 38400 tty1
		done
		exit 0
	fi

	SCRIPTS_DIR=$(dirname $ORIGIN)/scripts.d

	for each in "$SCRIPTS_DIR"/*; do
		run_script "$each"
	done
}

remote_boot(){
	udhcpc -s /usr/lib/scripty/udhcpc-boot.script
	for each in $(cat /tmp/udhcpc-bootp-info); do
		eval $each
	done
	if [ -z "$siaddr" ]; then
		echo "Server IP not found in udhcpc variables"
		return
	fi
	if [ -z "$base" ]; then
		base=/scripty
	fi
	mkdir /root/remote
	cd /root/remote
	tftp -g -r "$base"/list "$siaddr"
	for each in $(cat list); do
		rm -f "$each"
		tftp -g -r "$base/scripts.d/$each" "$siaddr"
		sh "$each"
	done
	
}
BOOT_MODE=local
for each in $(cat /proc/cmdline); do
	key=$(echo $each | cut -d= -f1)
	value=$(echo $each | cut -d= -f2)
	eval $key=$value
	if [ "$key" = "remote" ]; then
		BOOT_MODE=remote
	fi
done

eval ${BOOT_MODE}_boot
