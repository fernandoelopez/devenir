HTTPFS2_VERSION = master
HTTPFS2_SITE = git://github.com/fernandolopez/httpfs2.git
HTTPFS2_SITE_METHOD = git

#HTTPFS2_CFLAGS = -I$(LINUX_HEADERS_DIR)/include $(TARGET_CFLAGS)

define HTTPFS2_BUILD_CMDS
	#$(MAKE) -C $(@D) CFLAGS="$(HTPFS2_UTIL_CFLAGS)" CC="$(TARGET_CC)" LD="$(TARGET_LD)" all
	$(MAKE) -C $(@D) CC="$(TARGET_CC)" LD="$(TARGET_LD)" all
endef


define HTTPFS2_INSTALL_TARGET_CMDS
	$(MAKE) -C $(@D) DESTDIR=$(TARGET_DIR) install
endef

$(eval $(call GENTARGETS,package,httpfs2))
